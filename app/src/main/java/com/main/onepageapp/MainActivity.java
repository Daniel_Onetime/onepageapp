package com.main.onepageapp;


import java.io.BufferedWriter;
import java.io.IOException;
//import okhttp3.MediaType;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.RequestBody;
//import okhttp3.Response;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity
{

    private EditText first_name_edt,last_name_edt,dob_name_edt,unique_name_edt,eventid_name_edt;
    private String first_name_edt_str="",last_name_edt_str="",dob_name_edt_str="",unique_name_str="",eventid_name_str="",gender_str="Male";
    private RadioGroup genderrb;
    private Button submit_but;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        idinit();
        clickevent();
    }

    private void clickevent() {
        submit_but.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                first_name_edt_str= first_name_edt.getText().toString();
                last_name_edt_str=  last_name_edt.getText().toString();
                dob_name_edt_str=  dob_name_edt.getText().toString();
                unique_name_str= unique_name_edt.getText().toString();
                eventid_name_str= eventid_name_edt.getText().toString();

                if (first_name_edt_str==null||first_name_edt_str.equalsIgnoreCase(""))
                {
                    Toast.makeText(MainActivity.this,getResources().getString(R.string.enterfirstname),Toast.LENGTH_LONG).show();
                }
                else if (last_name_edt_str==null||last_name_edt_str.equalsIgnoreCase(""))
                {
                    Toast.makeText(MainActivity.this,getResources().getString(R.string.enterlastname),Toast.LENGTH_LONG).show();
                }
                else if (dob_name_edt_str==null||dob_name_edt_str.equalsIgnoreCase(""))
                {
                    Toast.makeText(MainActivity.this,getResources().getString(R.string.enteryourdateofbirth),Toast.LENGTH_LONG).show();
                }
                else if (unique_name_str==null||unique_name_str.equalsIgnoreCase(""))
                {
                    Toast.makeText(MainActivity.this,getResources().getString(R.string.enteryouruniqueid),Toast.LENGTH_LONG).show();
                }
                else if (eventid_name_str==null||eventid_name_str.equalsIgnoreCase(""))
                {
                    Toast.makeText(MainActivity.this,getResources().getString(R.string.enteryoureventid),Toast.LENGTH_LONG).show();
                }
                else
                {
                    int gen_id =  genderrb.getCheckedRadioButtonId();
                    if (gen_id==R.id.malerb)
                    {
                        gender_str="Male";
                    }
                    else
                    {
                        gender_str="Female";
                    }


                    String ipAddress="http://dashboard.movingisimproving.life/api/rawtagmap";
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("firstname",first_name_edt_str);
                        jsonObject.put("lastname",last_name_edt_str);
                        jsonObject.put("dob",dob_name_edt_str);
                        jsonObject.put("sex",gender_str);
                        jsonObject.put("tagname",unique_name_str);
                        jsonObject.put("eventid",eventid_name_str);


                        new InteractWithServer().execute(ipAddress,jsonObject.toString());
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }


               }
            }
        });
    }

    private void idinit()
    {
        submit_but = findViewById(R.id.submit_but);
        first_name_edt = findViewById(R.id.first_name_edt);
        last_name_edt = findViewById(R.id.last_name_edt);
        dob_name_edt = findViewById(R.id.dob_name_edt);
        unique_name_edt = findViewById(R.id.unique_name_edt);
        eventid_name_edt = findViewById(R.id.eventid_name_edt);
        genderrb = findViewById(R.id.genderrb);
        progressbar = findViewById(R.id.progressbar);
    }







    public class InteractWithServer extends AsyncTask<String, Void,String>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {

            String result = "";

            HttpURLConnection httpURLConnection=null;

            try {
                httpURLConnection = (HttpURLConnection) new URL(strings[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestProperty("Content-Type", "application/json");

                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(strings[1]);
                writer.close();
                os.close();


                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1)
                {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    result += current;

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            Log.d("RESULT: ",s);
            super.onPostExecute(s);
            progressbar.setVisibility(View.GONE);

            s = s.replaceAll(" ", "");
            if(s.equals("200"))
            {
                Toast.makeText(MainActivity.this,"Data Submitted Successfully",Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(MainActivity.this,"Error: " + s,Toast.LENGTH_LONG).show();
            }
        }

    }
}
